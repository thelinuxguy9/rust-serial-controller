
// Copyright (c) 2023 LGuy9

#![feature(exit_status_error)]

use std::process::Command;
use std::io;
use serialport;

use std::time;
use std::time::*;
use std::sync::mpsc::{Sender, Receiver};
use std::sync::mpsc;
use std::thread;

// commands
enum Tcommand {
    RAISE_VOLUME,
    LOWER_VOLUME,
    TOGGLE_MUTE,
    PLAY,
    PAUSE,

    VIM_UP,
    VIM_DOWN,
    VIM_RIGHT,
    VIM_LEFT,
    QUIT,

    LIGHT_CONTROL,

    NEW_TAB,
    CLOSE_TAB,

    FULLSCREEN,
}

enum Codes {
    ON,
    MUTE,

    OK,
    UP,
    DOWN,
    LEFT,
    RIGHT,
    VOLUME_UP,
    VOLUME_DOWN,

    ONE,
    TWO,
    THREE,
    FOUR,
    FIVE,
    SIX,
    SEVEN,
    EIGHT,
    NINE,
    ZERO,
    REPEAT,

    PLAY,
    PAUSE,
    STOP,
    REC,
    BACKWARD,
    FORWARD,
    NEXT,
    PREVIOUS,

    RED,
    GREEN,
    YELLOW,
    BLUE,
}

/*
enum Codes {
    FF30CF,
    FFE21D,

    FFA857,
    FF28D7,
    FF6897,
    FFB847,
    FF8877,
    FF3AC5,
    FFBA45,

    FF42BD,
    FF827D,
    FF02FD,
    FF629D,
    FFA25D,
    FF22DD,
    FF52AD,
    FF926D,
    FF12ED,
    FFB24D,
    FFFFFFFF,

    FF2AD5,
    FFA05F,
    FFC837,
    FFD02F,
    FF40BF,
    FF728D,
    FFC03F,
    FFE01F,

    FF00FF,
    FF1AE5,
    FF807F,
    FF5AA5,
}
*/

fn read(rx: &Receiver<i32>, name: &str, baud: u32) {
    println!("reading from device: {} with baud rate: {}", name, baud);

    while rx.try_recv().unwrap_or_else(|_| { 1 }) == 1 {
        let open_port = serialport::new(name, baud)
            .timeout(Duration::from_millis(300))
            .open();

        let open_port = match open_port {
            Ok(mut port) => {
                while rx.try_recv().unwrap_or_else(|_| { 1 }) == 1 {
                    //let mut serial_buf = String::new();
                    let mut buffer: [u8; 32] = [0; 32];

                    //let read_text = port.read_to_string(&mut serial_buf).unwrap_or_else(|error| {
                    let read_text = port.read(&mut buffer).unwrap_or_else(|error| {
                        if error.to_string() == "Broken pipe" {
                            //println!("error: {}", error.to_string());
                            return 1;
                        }
                        thread::sleep(time::Duration::from_millis(300));
                        return 0;
                    });

                    if read_text == 1 {
                        break;
                    }

                    //for i in buffer {
                        //print!("{} ", i);
                    //}
                    //println!();

                    if buffer[0] != 0 {
                        let s = match std::str::from_utf8(&buffer) {
                            Ok(v) => v,
                            Err(e) => "ERROR",
                        };

                        //println!("buffer STRING: \"{}\"", s);
                        println!("buffer STRING: \"{}\"", &s[..6]);

                        match &s[..6] {
                            "FF30CF" => { println!("ON"); run_command(Tcommand::NEW_TAB); },
                            "FFE21D" => { println!("MUTE"); run_command(Tcommand::TOGGLE_MUTE); },

                            "FFA857" => { println!("OK"); run_command(Tcommand::FULLSCREEN); },
                            "FF28D7" => { println!("UP"); run_command(Tcommand::VIM_UP); },
                            "FF6897" => { println!("DOWN"); run_command(Tcommand::VIM_DOWN); },
                            "FF8877" => { println!("RIGHT"); run_command(Tcommand::VIM_RIGHT); },
                            "FFB847" => { println!("LEFT"); run_command(Tcommand::VIM_LEFT); },
                            "FF3AC5" => { println!("VOLUEM_UP"); run_command(Tcommand::RAISE_VOLUME); },
                            "FFBA45" => { println!("VOLUEM_DOWN"); run_command(Tcommand::LOWER_VOLUME); },

                            "FF42BD" => { println!("ONE"); },
                            "FF827D" => { println!("TWO"); },
                            "FF02FD" => { println!("THREE"); },
                            "FF629D" => { println!("FOUR"); },
                            "FFA25D" => { println!("FIVE"); },
                            "FF22DD" => { println!("SIX"); },
                            "FF52AD" => { println!("SEVEN"); },
                            "FF926D" => { println!("EIGHT"); },
                            "FF12ED" => { println!("NINE"); },
                            "FFB24D" => { println!("ZERO"); },
                            //"FFFFFFFF" => { println!("REPEAT"); },

                            "FF2AD5" => { println!("PLAY"); run_command(Tcommand::PLAY); },
                            "FFA05F" => { println!("PAUSE"); run_command(Tcommand::PAUSE); },
                            "FFC837" => { println!("STOP"); },
                            "FFD02F" => { println!("REC"); },
                            "FF40BF" => { println!("BACKWARD"); },
                            "FF728D" => { println!("FORWARD"); },
                            "FFC03F" => { println!("PREVIOUS"); },
                            "FFE01F" => { println!("NEXT"); },

                            "FF00FF" => { println!("RED"); },
                            "FF1AE5" => { println!("GREEN"); },
                            "FF807F" => { println!("YELLOW"); },
                            "FF5AA5" => { println!("BLUE"); },

                            "FF7887" => { println!("MENU"); run_command(Tcommand::LIGHT_CONTROL); },
                            "FF58A7" => { println!("EXIT"); run_command(Tcommand::QUIT); },
                            "FFCA35" => { println!("INFO");},
                            "FF32CD" => { println!("EPG"); },
                            "FFDA25" => { println!("FAV"); },
                            "FF50AF" => { println!("SLEEP"); },
                            "FF9867" => { println!("AUDIO"); },
                            "FFE817" => { println!("RECALL"); },
                            "FF0AF5" => { println!("DEV"); },
                            "FF708F" => { println!("AB"); },
                            "FFAA55" => { println!("TV"); },
                            "FF48B7" => { println!("SAT"); },

                            "FFC23D" => { println!("F1"); },
                            "FFD827" => { println!("F2"); },

                            _ => {},
                        };

                        /*
                        if !serial_buf.is_empty() {
                            //println!("string: \"{}\"", serial_buf);

                            print!("STRING: ");

                            let received_commands: Vec<&str> = serial_buf.lines().collect();

                            //for i in received_commands {
                                //println!("i: \"{}\"", i);
                            //}

                        }
                        */
                    }

                    /*
                    let read_text = port.read_to_string(&mut serial_buf);

                    match read_text {
                        Ok(_) => {
                            print!("buffer: {}", serial_buf);
                        },
                        Err(e) => {
                            //println!("error: {}", e);
                            //thread::sleep(time::Duration::from_secs(1));
                        }
                    }
                    */
                }
            },
            Err(_) => {
                //println!("Could not open device");
                thread::sleep(time::Duration::from_millis(300));
            },
        };

        std::mem::drop(open_port);
    }
}

fn init_serial(rx: Receiver<i32>) {
    while rx.try_recv().unwrap_or_else(|_| { 1 }) == 1 {
        let ports = serialport::available_ports().expect("No devices were found");

        for i in ports {
            if i.port_name.contains("ACM") {
                read(&rx, &String::from(i.port_name), 500000);
                break;
            }
        }
        thread::sleep(time::Duration::from_millis(300));
    }
}

fn run_command(command: Tcommand) {
    let command_result =
        match command {
            Tcommand::RAISE_VOLUME => Command::new("pactl").arg("set-sink-volume").arg("@DEFAULT_SINK@").arg("+5%").output(),
            Tcommand::LOWER_VOLUME => Command::new("pactl").arg("set-sink-volume").arg("@DEFAULT_SINK@").arg("-5%").output(),
            Tcommand::TOGGLE_MUTE => Command::new("pactl").arg("set-sink-mute").arg("@DEFAULT_SINK@").arg("toggle").output(),
            Tcommand::PLAY => Command::new("playerctl").arg("-a").arg("play").output(),
            Tcommand::PAUSE => Command::new("playerctl").arg("-a").arg("pause").output(),

            Tcommand::VIM_UP => Command::new("xdotool").arg("key").arg("super+k").output(),
            Tcommand::VIM_DOWN => Command::new("xdotool").arg("key").arg("super+j").output(),
            Tcommand::VIM_RIGHT => Command::new("xdotool").arg("key").arg("super+l").output(),
            Tcommand::VIM_LEFT => Command::new("xdotool").arg("key").arg("super+h").output(),
            Tcommand::QUIT => Command::new("xdotool").arg("key").arg("super+q").output(),
            Tcommand::FULLSCREEN => Command::new("xdotool").arg("key").arg("super+f").output(),

            Tcommand::LIGHT_CONTROL => Command::new("ldm").output(),

            Tcommand::NEW_TAB => Command::new("brave").arg("--new-tab").output(),
            _ => { println!("Command not implemented"); Command::new("").output() },
        };

    if command_result.is_err() {
        println!("error: {}", command_result.err().unwrap());
    } else if command_result.is_ok() {
        println!("result: {}", "all ok");
    }

    /*
    match command_result {
        Ok(result) => {
            println!("result: {}", result.success());
            match result.exit_ok() {
                Ok(_) => println!("result: {}", "all ok"),
                Err(test) => println!("result: {}", test),
            }
            match result.code() {
                Some(test) => println!("result: {}", test),
                None => {},
            }
        },
        Err(error) => println!("error: {}", error),
    }
    */
}

fn main() {
    let mut running = true;
    let (tx, rx): (Sender<i32>, Receiver<i32>) = mpsc::channel();

    let serial_thread = thread::spawn(move || {
        init_serial(rx);
    });

    let stdin = io::stdin();
    let input = &mut String::new();

    while running {
        input.clear();
        let result = stdin.read_line(input);

        match result {
            Ok(_) => {
                //println!("{}", input);

                match input.replace("\n", "").as_str() {
                    "raise volume" => { run_command(Tcommand::RAISE_VOLUME) },
                    "lower volume" => { run_command(Tcommand::LOWER_VOLUME) },
                    "toggle mute" => { run_command(Tcommand::TOGGLE_MUTE) },
                    "new tab" => { run_command(Tcommand::NEW_TAB) },
                    m => {
                        println!("Wrong command received: \"{}\"", m);
                        //running = false;
                    }
                }

                if input.contains("exit") {
                    println!("exit command received, exiting...");
                    running = false;
                }
            },
            Err(error) => {
                println!("An error occurred: {}, exiting...", error);
                running = false;
            },
        }
    }

    match tx.send(0) {
        Ok(_) => println!("Done"),
        Err(E) => println!("Error: {E}"),
    }

    match serial_thread.join() {
        Ok(_) => println!("Done"),
        Err(_) => println!("Something went wrong"),
    }
}
