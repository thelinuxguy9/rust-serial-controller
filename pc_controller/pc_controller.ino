
// Copyright (c) 2023 LGuy9

#include <IRremote.h>
//#include <IRremoteInt.h>
//#include <IRLibALL.h>

#define RECV_PIN                13
//#define IR_RECEIVER_PIN         13
//#define IR_SENDER_PIN           3

IRrecv receiver(RECV_PIN);
decode_results results;
//IRsend sender;

void setup()
{
        //Serial.begin(115200);
        //Serial.begin(2000000);
        Serial.begin(500000);
        receiver.enableIRIn();
        receiver.blink13(false);
}

void loop()
{
    if (receiver.decode(&results)) {
        Serial.print(results.value, HEX);
        receiver.resume();
    }

    /*
    if (receiver.decode()) {
        Serial.print(receiver.decodedIRData.decodedRawData, HEX);
        receiver.resume();
    }
    */
}

