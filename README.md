## Introduction:

Rust Serial Controller is a program that allows you to control your PC with a remote control.

I created this in my spare time since I wanted to control the playback of music and videos on my PC
as if it were a TV, I wanted to have the ability to change the volume, and maybe mute and unmute.

Untile I learnt about playerctl, which allows you to play, pause, and do many many more from the terminal.

It also works with many clients, like Youtube, which previously I thought would require a selenium script.

I implemented some basic functions to play, pause, mute, and change the volume from the remote controller.

I also added some functions to interact with windows in my dwm install, it has the ability to killing a window,

making it fullscreen and more.

You can also add custom commands to your heart's content.

If there is enough intrest, maybe we can add modes, or a states similar to VIM motions.

Maybe write a remote control configuration script, add Wayland support.

Maybe add some demu scripts to do all kinds of stuff.

## Build instructions:

Just run `cargo build` to build the app itself.

Then `cd pc_controller` code directory and run `make` to compile and upload the code to your arduino.

## Support, bugs, developent, and feedback:

If you have any problem running the software, or any suggestion or requests,
and if you would like to support the software, see CONTRIBUTING.md.

## License:

GNU General Public License, version 2 (GPL-2.0)
